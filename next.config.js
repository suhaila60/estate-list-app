/** @type {import('next').NextConfig} */
const nextConfig = {
    async redirects() {
        return [
          {
            source: "/",
            destination: "/login",
            permanent: true,
          },
         
        ];
      },
     images: {
        unoptimized: true,
      }, 
    
}

module.exports = nextConfig
