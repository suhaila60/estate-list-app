import React from "react";
import { TableCell, TableRow } from "@mui/material";
import Image from "next/image";
import Link from "next/link";
import ClearIcon from "@mui/icons-material/Clear";
import ChevronRightSharpIcon from "@mui/icons-material/ChevronRightSharp";
import { Property } from "../types/Estate";
interface FavoriteCardProps {
  property: Property;
  handleClick: (id: number) => void;
}
const myPropertyCard: React.FC<FavoriteCardProps> = ({
  property,
  handleClick,
}) => {
  return (
    <TableRow key={property?.id}>
      <TableCell>
        <Image
          src={property?.images?.[0]}
          alt={"estate"}
          width={40}
          height={40}
        />
      </TableCell>
      <TableCell>{property?.location?.city}</TableCell>
      <TableCell>{property?.price}</TableCell>

      <TableCell>
        {property.id !== undefined && (
          <ClearIcon onClick={() => handleClick(Number(property.id))} />
        )}{" "}
      </TableCell>
      <TableCell>
        <Link
          href={`/estate/${property?.id}`}
          style={{ textDecoration: "none" }}
        >
          <ChevronRightSharpIcon />
        </Link>
      </TableCell>
    </TableRow>
  );
};

export default myPropertyCard;
