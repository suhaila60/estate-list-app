import React from 'react'
import { Box } from '@mui/material'

const HomeServiceSection = () => {
  return (
    <Box
        id="services"
        sx={{
          padding: '20px',
          border: 'solid white 1px ',
          backgroundColor: 'secondary.light',
          marginTop: '80px',
          color: 'primary.light',
          borderRadius: '10px',
          width: '40%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          textAlign: 'center',
          lineHeight: 2,
        }}
      >
        <h2>Our Services</h2>
        <p>
          Discover a range of services tailored to make your real estate journey
          seamless. Whether you are buying, selling, or renting, our platform
          provides you with the tools and information you need.
        </p>
        <p>
          Explore a curated list of properties, get detailed information, and
          connect with property owners effortlessly. Join our community and
          experience the future of real estate.
        </p>
      </Box>
  )
}

export default HomeServiceSection