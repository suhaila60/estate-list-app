'use client'
import React, { useEffect } from "react";
import { Box, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getEstates } from "../redux/slice/estateSlice";
import { RootState } from "../redux/store";
import { addToFavorite } from "../redux/slice/favoriteSlice";
import EstateCard from "./EstateCard";
import Pagination from "./Pagination";
import { Property } from "../types/Estate";
import Loadings from "./Loading";

const EstateList = () => {
  const dispatch = useDispatch();
  const { estate, isLoading, error } = useSelector((state: RootState) => state.estates);

  useEffect(() => {
    const handleEstate = () => {
      dispatch(getEstates());
    };
    handleEstate();
  }, [dispatch]);

 

  if (isLoading) {
    return <Loadings/>;
  }

  if (error) {
    return <Typography>Error: {error}</Typography>;
  }

  return (
    <>
      <Box display="flex" flexWrap="wrap" justifyContent="center" gap={2}>
        {estate?.map((data:Property) => (
          <EstateCard key={data?.id} data={data}  />
        ))}
      </Box>
      <Pagination />
    </>
  );
};

export default EstateList;
