'use client'
import React from 'react'
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { Button } from '../../../node_modules/@mui/material/index';
interface PageProps {
    page: {
      name: string;
      link: string;
    };
    handleCloseNavMenu: () => void;
  }
const HeaderNavLinks: React.FC<PageProps> = ({page,handleCloseNavMenu}) => {
    const pathName = usePathname();

  return (
                  <Button
                key={page.name}
                onClick={handleCloseNavMenu}
                style={{ my: 2, color: pathName.includes(page.link)
                    ? "rgb(135,206,250)"
                    : "white",  }}
                href={page.link}
              >
                {page.name}
              </Button>
           )
}

export default HeaderNavLinks