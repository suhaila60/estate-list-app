import React, { useState } from "react";
import { Button } from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import { useRouter } from "next/navigation";
import { AppDispatch, RootState } from "../redux/store";
import {
  useDispatch,
  useSelector,
} from "../../../node_modules/react-redux/dist/react-redux";
import { createProperty } from "../redux/slice/myPropertySlice";
import Loadings from "./Loading";
import { addEstates } from "../redux/slice/estateSlice";
import { Property } from "../types/Estate";
const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
interface CreateProperty {
  id: string;
  type: string;
  location: {
    city: string;
    state: string;
    country: string;
    latitude: number;
    longitude: number;
  };
  images: string[];
  price: string;
  description: string;
  features: string[];
  owner: {
    name: string;
    contact: string;
    phone: string;
  };
}
const AddPropertyButton = ({ inputData }: { inputData: CreateProperty }) => {
  const router = useRouter();
  const [openSnackbar, setOpenSnackbar] = useState<boolean>(false);
  const { myProperty, isLoading, error } = useSelector(
    (state: RootState) => state.myProperties
  );
  const dispatch: AppDispatch = useDispatch();
  function handleCreate() {
    dispatch(createProperty(inputData));
    dispatch(addEstates(inputData));
    setOpenSnackbar(true);
    if (myProperty) {
      const timer = setTimeout(() => {
        router.push("/my-property");
      }, 3000);

      return () => clearTimeout(timer);
    }
  }

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) =>
    reason === "clickaway" ? undefined : setOpenSnackbar(false);
  return (
    <>
      <Button variant="contained" onClick={handleCreate}>
        Add property
      </Button>
      <Snackbar
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleClose}
      >
        <Alert
          onClose={handleClose}
          severity={isLoading ? "info" : error ? "error" : "success"}
          sx={{ width: "100%" }}
        >
          {isLoading ? (
            <Loadings />
          ) : error ? (
            error
          ) : myProperty ? (
            "property created successfully"
          ) : (
            ""
          )}
        </Alert>
      </Snackbar>
    </>
  );
};

export default AddPropertyButton;
