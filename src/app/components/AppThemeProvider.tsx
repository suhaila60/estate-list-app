"use client";
import React from "react";
import { createTheme, ThemeProvider } from "@mui/material";

const AppThemeProvider = ({ children }: { children: React.ReactNode }) => {
  const theme = createTheme({
    palette: {
      primary: {
        light: "#FFFFFF",
        main: "#030552",
        dark: "#6331f7",
      },
      secondary: {
        light: "rgba(3, 5, 82, 0.5)",
        main: "#441552",
        dark: "#a938eb ",
      },
      error: {
        main: "#590a0c",
      },
      success: {
        main: "#042405",
      },
    },
  });
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default AppThemeProvider;
