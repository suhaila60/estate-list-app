import React from 'react'
import { TableCell, TableRow } from '@mui/material'
import ClearIcon from "@mui/icons-material/Clear";
import ChevronRightSharpIcon from "@mui/icons-material/ChevronRightSharp";
import Image from "next/image";
import Link from "next/link";
import { Property } from '../types/Estate';
interface FavoriteCardProps {
    favorites: Property ;
    handleClick: (id: number ) => void;
  }
const FavoriteCard: React.FC<FavoriteCardProps> = ({favorites, handleClick}) => {
  return (
    <TableRow key={favorites?.id}>
                  <TableCell>
                    <Image
                      src={favorites?.images?.[0] || "fallback_image_url"}
                      alt={"estate"}
                      width={40}
                      height={40}
                    />
                  </TableCell>
                  <TableCell>{favorites?.location?.city}</TableCell>
                  <TableCell>{favorites?.price}</TableCell>

                  <TableCell>
                 
          <ClearIcon onClick={() => handleClick(favorites.id)} />
                        </TableCell>
                  <TableCell>
                    <Link
                      href={`/estate/${favorites?.id}`}
                      style={{ textDecoration: "none" }}
                    >
                      <ChevronRightSharpIcon />
                    </Link>
                  </TableCell>
                </TableRow>
  )
}

export default FavoriteCard