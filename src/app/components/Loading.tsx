import * as React from 'react';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';

export default function Loadings() {
  return (
    <Grid item sx={{ color: 'grey.500', minHeight: "82vh" }}  
    display="flex" justifyContent="center" alignItems="center">
      <CircularProgress color="success" />
    </Grid>
  );
}