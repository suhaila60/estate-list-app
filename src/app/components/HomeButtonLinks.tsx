import React from 'react'
import { Box, Button } from '@mui/material';
interface ButtonData {
    text: string;
    link: string;
  }
const buttons: ButtonData[] = [
    { text: 'Services', link: '#services' },
    { text: 'Contact Us', link: '#contact-us' },
    { text: 'Find Property', link: '/estate' },
  ];

const HomeButtonLinks = () => {
  return (
    <Box mt="70px" display="flex">
    {buttons.map((button) => (
      <Button 
        key={button.text}
        variant="outlined"
        sx={{ margin: 2, color: 'white', display: 'block' }}
        href={button.link}
      >
        {button.text}
      </Button>
    ))}
  </Box>
  )
}

export default HomeButtonLinks