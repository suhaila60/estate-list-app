'use client'
import React, { useState } from 'react'
import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Typography } from '../../../node_modules/@mui/material/index'
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import { Property } from '../types/Estate';
import { useDispatch } from '../../../node_modules/react-redux/dist/react-redux';
import { addToFavorite } from '../redux/slice/favoriteSlice';
interface FavoriteCardProps {
    data: Property ;
  }
const EstateCard : React.FC<FavoriteCardProps> = ({data}) => {
    const dispatch = useDispatch();

    const [isFavorite, setIsFavorite] = useState(false);
    const handleClick = (data: Property) => {
        dispatch(addToFavorite(data));
        setIsFavorite(!isFavorite);
      };
  return (
    <Card key={data?.id} sx={{ maxWidth: 345 }}>
    <CardActionArea>
      <CardMedia
        component="img"
        height="140"
        image={data?.images?.[0]}
        alt="Property Image"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {data.price}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {data.description}
        </Typography>
      </CardContent>
    </CardActionArea>
    <CardActions>
      <Button
        variant="contained"
        href={`/estate/${data.id}`}
        size="small"
        color="primary"
      >
        Show More
      </Button>
     
      <FavoriteBorderIcon
        onClick={() => handleClick(data)}
        variant="contained"
        href={`/favourites`}
        size="small"
        style={{ color: isFavorite ? 'red' : 'black' }}
      /> 
    </CardActions>
  </Card>
  )
}

export default EstateCard