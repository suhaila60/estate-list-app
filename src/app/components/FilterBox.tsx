'use client'
import React, { useState } from 'react';
import { Box, Button, Menu, MenuItem, Divider } from '@mui/material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

interface Option {
  name: string;
  options: string[];
}

const FilterBox: React.FC = () => {
  const categoryData: Option[] = [
    { name: 'type', options: ['land', 'apartment', 'house'] },
    { name: 'price', options: ['from 1,00000 to 3,00000 ', 'from 3,00000 to 8,00000', 'above 8,00000'] },
    { name: 'location', options: ['kerala', 'tamilnadu', 'mumbai'] },
  ];

  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [selectedCategory, setSelectedCategory] = useState<Option | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>, category: Option) => {
    setAnchorEl(event.currentTarget);
    setSelectedCategory(category);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setSelectedCategory(null);
  };

  return (
    <Box>
      <h3>Filter by</h3>
      <div>
        {categoryData.map((category, index) => (
          <React.Fragment key={index}>
            <Button
              id={`category-button-${index}`}
              aria-controls={`category-menu-${index}`}
              aria-haspopup="true"
              aria-expanded={selectedCategory === category ? 'true' : 'false'}
              disableElevation
              onClick={(e) => handleClick(e, category)}
              endIcon={<KeyboardArrowDownIcon />}
              sx={{ marginTop: '20px' }}
            >
              {category.name}
            </Button>
            <Divider />
          </React.Fragment>
        ))}
        <Menu
          id={selectedCategory ? `category-menu-${categoryData.indexOf(selectedCategory)}` : undefined}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {selectedCategory && selectedCategory.options.map((option, optionIndex) => (
            <MenuItem key={optionIndex} onClick={handleClose}>
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
    </Box>
  );
};

export default FilterBox;
