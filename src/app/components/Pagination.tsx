import React from 'react'
import { Box, Button } from '@mui/material'

const Pagination = () => {
  return (
    <Box display="flex" justifyContent="space-between" mt={2} px={2}>
        <Button variant="contained" color="primary">
          Previous
        </Button>
        <Button variant="contained" color="primary">
          Next
        </Button>
      </Box>
  )
}

export default Pagination