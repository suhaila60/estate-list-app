import React from 'react'
import { Box } from '../../../node_modules/@mui/material/index'
interface ContactDetails {
    email: string;
    phone: string;
    address: string;
  }
const HomeContactSection = () => {
    const contactDetails:ContactDetails = {
        email: 'info@example.com',
        phone: '+1 123-456-7890',
        address: '123 Main Street, Cityville',
      };
  return (
    <Box
    id="contact-us"
    sx={{
      padding: '20px',
      border: 'solid white 1px ',
      backgroundColor: 'secondary.light',
      marginTop: '80px',
      color: 'primary.light',
      borderRadius: '10px',
      width: '40%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      textAlign: 'center',
      lineHeight: 2,
    }}
  >
    <h2>Contact Us</h2>
    <p>
      Have questions or need assistance? Reach out to us!
    </p>
    <p>Email: {contactDetails.email}</p>
    <p>Phone: {contactDetails.phone}</p>
    <p>Address: {contactDetails.address}</p>
  </Box>  )
}

export default HomeContactSection