"use client";
import * as React from "react";
import Box from "@mui/material/Box";
import BottomNavigation from "@mui/material/BottomNavigation";
import Link from "next/link";

export default function SimpleBottomNavigation() {
  const [value, setValue] = React.useState(0);

  return (
    <Box
      sx={{
        position: "absolute",
        bottom: 0,
        width: "100%",
        color: "primary.light",
      }}
    >
      <BottomNavigation
        sx={{
          backgroundColor: "primary.main",
          display: "flex",
          justifyContent: "space-evenly",
          alignItems: "center",
        }}
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      >
        <Link href={"/about"}>about us</Link>
        <Link href={"/contact"}>contact us</Link>
        <Link href={"/follow"}>follow us</Link>
      </BottomNavigation>
    </Box>
  );
}
