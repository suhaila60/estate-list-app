"use client";
import React from "react";
import { Grid } from "@mui/material/index";
import { Paper, TextField } from "@mui/material/index";
import { Button } from "@mui/material/index";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { createRegistration } from "../redux/slice/registerSlice";

const Register = () => {
  const [username, setUsername] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const dispatch = useDispatch();
  const handleRegister = async () => {
    const inputData = {
      username: username,
      email: email,
      password: password,
    };
    dispatch(createRegistration(inputData));
  };
  return (
    <Grid
      container
      alignContent="center"
      justifyContent="center"
      sx={{
        width: "100vw",
        height: "auto",
        padding: "25px",
      }}
    >
      <Paper
        elevation={10}
        sx={{
          width: "30%",
          height: "60vh",
          padding: "20px",
          borderRadius: "10px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <TextField
          required
          id="outlined-basic"
          label="user name"
          variant="outlined"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />

        <TextField
          required
          id="outlined-basic"
          label="email"
          variant="outlined"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />

        <TextField
          required
          type="password"
          id="outlined-required"
          label="password"
          variant="outlined"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />

        <Button
          sx={{ width: "150px" }}
          variant="contained"
          onClick={handleRegister}
        >
          Register
        </Button>
        <span>or login </span>
        <Link href={"/login"}>login</Link>
      </Paper>
    </Grid>
  );
};

export default Register;
