'use client'
import React, { useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import Paper from "@mui/material/Paper";
import TableRow from "@mui/material/TableRow";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../redux/store";
import { deleteFavoriteItem, getFavorites } from "../redux/slice/favoriteSlice";
import { Box } from "@mui/material/index";
import FavoriteCard from "../components/FavoriteCard";

const Favorite = () => {
  const dispatch: AppDispatch = useDispatch();

  useEffect(() => {
    const handleFavorites = () => {
      dispatch(getFavorites());
    };
    handleFavorites();
  }, [dispatch]);

  const handleClick = async (favoriteId: number) => {
    dispatch(deleteFavoriteItem(favoriteId));
    dispatch(getFavorites())

  };
  const { favorite, isLoading, error } = useSelector((state: RootState) => state.favorites);


  return (
    <Box display="flex" justifyContent="center" padding="20px">
      <TableContainer component={Paper} sx={{ width: "70%" }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <h3>Image</h3>
              </TableCell>
              <TableCell>
                <h3>location</h3>
              </TableCell>
              <TableCell>
                <h3>Price</h3>
              </TableCell>
              <TableCell>
                <h3>remove</h3>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Array.isArray(favorite) &&
              favorite?.map((favorites) => (
                <FavoriteCard handleClick={handleClick} favorites={favorites} key={favorites?.id} />
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default Favorite;
