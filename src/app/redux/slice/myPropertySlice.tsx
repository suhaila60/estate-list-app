import { MyPropertyService } from "@/app/services/myProperyService";
import { Property } from "@/app/types/Estate";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
interface MyPropertySlice {
  myProperty: Property[];
  isLoading: boolean;
  error: string | null;
}
const initialState = {
  myProperty: [],
  isLoading: true,
  error: null,
};
export const getMyProperties = createAsyncThunk(
  "myProperty/getMyProperties",
  async (_, thunkAPI) => {
    try {
      const data = await MyPropertyService.getMyProperties();
      return data;
    } catch (error) {
      throw error;
    }
  }
);
export const createProperty = createAsyncThunk(
  "myProperty/createProperty",
  async (propertyId, thunkAPI) => {
    try {
      const data = await MyPropertyService.CreateEstate(propertyId);
      return data;
    } catch (error) {
      throw error;
    }
  }
);
const asyncActions = [
  {
    action: createProperty,
    fulfilled: (state, action) => {
      state.myProperty = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
  {
    action: getMyProperties,
    fulfilled: (state, action) => {
      state.myProperty = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
];

export const myPropertySlice = createSlice({
  name: "myProperties",
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    asyncActions.forEach(({ action, fulfilled, pending, rejected }) => {
      builder.addCase(action.fulfilled, fulfilled);
      builder.addCase(action.pending, pending);
      builder.addCase(action.rejected, rejected);
    });
  },
});
export const {} = myPropertySlice.actions;
export default myPropertySlice.reducer;
