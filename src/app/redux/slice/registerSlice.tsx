import { RegistrationService } from "@/app/services/registerService";
import RegisterUser from "@/app/types/RegisterUser";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
interface RegisterUserSlice {
  registration: RegisterUser[];  
  isLoading: boolean;
  error: string | null;
}
const initialState:RegisterUserSlice = {
  registration: [],
  isLoading: true,
  error: null,
};
export const createRegistration = createAsyncThunk(
  "registrations/createRegistration",
  async (payload, thunkAPI) => {
    try {
      const data = await RegistrationService.createRegistration(payload);
      return data;
    } catch (error) {
      throw error;
    }
  }
);
export const getRegisterUser = createAsyncThunk(
  "registrations/getRegisterUser",
  async (_, thunkAPI) => {
    try {
      const data = await RegistrationService.getRegistration();
      return data;
    } catch (error) {
      throw error;
    }
  }
);

const asyncActions = [
  {
    action: createRegistration,
    fulfilled: (state, action) => {
      state.registration = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
  {
    action: getRegisterUser,
    fulfilled: (state, action) => {
      state.registration = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
];

export const registrationSlice = createSlice({
  name: "registrations",
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    asyncActions.forEach(({ action, fulfilled, pending, rejected }) => {
      builder.addCase(action.fulfilled, fulfilled);
      builder.addCase(action.pending, pending);
      builder.addCase(action.rejected, rejected);
    });
  },
});
export const {} = registrationSlice.actions;
export default registrationSlice.reducer;
