import { EstateService } from "@/app/services/estateService";
import { Property } from "@/app/types/Estate";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

interface EstateState {
  estate: Property[];
  isLoading: boolean;
  error: string | null;
}
const initialState:EstateState = {
  estate: [],
  isLoading: true,
  error: null,
};
export const getEstates = createAsyncThunk(
  "estates/getEstate",
  async (_, thunkAPI) => {
    try {
      const data = await EstateService.getEstates();
      return data;
    } catch (error) {
      throw error;
    }
  }
);
export const estateDetails = createAsyncThunk(
  "estates/estateDetails",
  async (estateId, thunkAPI) => {
    try {
      const data = await EstateService.getSingleEstate(estateId);
      return data;
    } catch (error) {
      throw error;
    }
  }
);
export const addEstates = createAsyncThunk(
  "estates/addEstates",
  async (propertyId, thunkAPI) => {
    try {
      const data = await EstateService.AddEstate(propertyId);
      return data;
    } catch (error) {
      throw error;
    }
  }
);
const asyncActions = [
  {
    action: getEstates,
    fulfilled: (state, action) => {
      state.estate = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
  {
    action: estateDetails,
    fulfilled: (state, action) => {
      state.estate = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
  {
    action: addEstates,
    fulfilled: (state, action) => {
      state.estates = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
];

export const estateSlice = createSlice({
  name: "estates",
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    asyncActions.forEach(({ action, fulfilled, pending, rejected }) => {
      builder.addCase(action.fulfilled, fulfilled);
      builder.addCase(action.pending, pending);
      builder.addCase(action.rejected, rejected);
    });
  },
});
export const {} = estateSlice.actions;
export default estateSlice.reducer;
