import { createSlice } from "@reduxjs/toolkit";

interface FilterState {
  type: string | null;
  price: string | null;
  location: string | null;
  searchText: string | null;
}

const initialState: FilterState = {
  type: null,
  price: null,
  location: null,
  searchText: null,
};

const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    setFilterSearchText: (state, action) => {
      state.type = action.payload;
    },
    setFilterPrice: (state, action) => {
      state.price = action.payload;
    },
    setFilterLocation: (state, action) => {
      state.location = action.payload;
    },
  },
});

export const { setFilterSearchText, setFilterPrice, setFilterLocation } =
  filterSlice.actions;
export default filterSlice.reducer;
