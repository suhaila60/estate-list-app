import {} from "@/app/services/estateService";
import { FavoriteService } from "@/app/services/favoriteService";
import { Property } from "@/app/types/Estate";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
interface FavoriteState {
  favorite: Property[];
  isLoading: boolean;
  error: string | null;
}
const initialState:FavoriteState = {
  favorite: [],
  isLoading: true,
  error: null,
};
export const addToFavorite = createAsyncThunk(
  "favorites/addToFavorite",
  async (estateId, thunkAPI) => {
    try {
      const data = await FavoriteService.addFavorites(estateId);
      return data;
    } catch (error) {
      throw error;
    }
  }
);
export const getFavorites = createAsyncThunk(
  "favorites/getFavorites",
  async (_, thunkAPI) => {
    try {
      const data = await FavoriteService.getFavorites();
      return data;
    } catch (error) {
      throw error;
    }
  }
);
export const deleteFavoriteItem = createAsyncThunk(
  "favorites/deleteFavoriteItem",
  async (favoriteId, thunkAPI) => {
    try {
      const data = await FavoriteService.deleteFavorite(favoriteId);
      return data;
    } catch (error) {
      throw error;
    }
  }
);

const asyncActions = [
  {
    action: addToFavorite,
    fulfilled: (state, action) => {
      state.favorites = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
  {
    action: getFavorites,
    fulfilled: (state, action) => {
      state.favorite = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },

  {
    action: deleteFavoriteItem,
    fulfilled: (state, action) => {
      state.favorite = action.payload;
      state.isLoading = false;
      state.error = null;
    },
    pending: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    rejected: (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    },
  },
];

export const favoriteSlice = createSlice({
  name: "estates",
  initialState,
  reducers: {},

  extraReducers: (builder) => {
    asyncActions.forEach(({ action, fulfilled, pending, rejected }) => {
      builder.addCase(action.fulfilled, fulfilled);
      builder.addCase(action.pending, pending);
      builder.addCase(action.rejected, rejected);
    });
  },
});
export const {} = favoriteSlice.actions;
export default favoriteSlice.reducer;
