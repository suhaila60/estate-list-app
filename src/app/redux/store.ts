import { configureStore } from "@reduxjs/toolkit";
import estateReducer from "./slice/estateSlice";
import favoriteReducer from "./slice/favoriteSlice";
import filterReducer from "./slice/filterSlice";
import myPropertyReducer from "./slice/myPropertySlice";
import registrationReducer from "./slice/registerSlice";

export const store = configureStore({
  reducer: {
    estates: estateReducer,
    favorites: favoriteReducer,
    myProperties: myPropertyReducer,
    registrations: registrationReducer,
    filter: filterReducer,
  },
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
