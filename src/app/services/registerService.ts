import RegisterUser from "../types/RegisterUser";
import { ServiceBase } from "./serviceBase";
export class RegistrationService extends ServiceBase {
  static createRegistration = async (payload:RegisterUser) => {
    const response = await fetch(this.getUrl("/register"), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };

  static getRegistration = async () => {
    const response = await fetch(this.getUrl(`/register`), {});

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };
}
