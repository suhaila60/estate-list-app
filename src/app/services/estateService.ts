import { Property } from "../types/Estate";
import { ServiceBase } from "./serviceBase";
export class EstateService extends ServiceBase {
  static getEstates = async () => {
    const response = await fetch(this.getUrl(`/estates`), {
      next: { revalidate: 0 },
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json()
  };
  static getSingleEstate = async (estateId:number) => {
    const response = await fetch(this.getUrl(`/estates/${estateId}`));

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message[0]);
    }
    return await response.json()
  };
  static AddEstate = async (payload:Property) => {
    const response = await fetch(this.getUrl("/estates"), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json()
  };
};
