export abstract class ServiceBase {
    static API_URL = "http://localhost:4000";
    static getUrl(path: string) {
      return `${this.API_URL}${path}`;
    }
  }