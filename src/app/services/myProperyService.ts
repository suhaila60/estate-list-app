import { Property } from "../types/Estate";
import { ServiceBase } from "./serviceBase";
export class MyPropertyService extends ServiceBase {
  static getMyProperties = async () => {
    const response = await fetch(this.getUrl(`/property`), {
      next: { revalidate: 0 },
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };
    static CreateEstate = async (payload:Property) => {
    const response = await fetch(this.getUrl("/property"), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };
}
