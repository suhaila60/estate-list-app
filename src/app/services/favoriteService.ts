import { Property } from "../types/Estate";
import { ServiceBase } from "./serviceBase";
export class FavoriteService extends ServiceBase {
  static addFavorites = async (payload:Property) => {
    const response = await fetch(this.getUrl("/wishlist"), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };

  static getFavorites = async () => {
    const response = await fetch(this.getUrl(`/wishlist`), {
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };
  static deleteFavorite = async (favoriteId:number) => {
    const response = await fetch(this.getUrl(`/wishlist/${favoriteId}`), {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      next: { revalidate: 0 },
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData?.message);
    }
    return await response.json();
  };
}
