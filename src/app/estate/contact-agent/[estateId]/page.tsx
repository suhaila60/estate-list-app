"use client";
import React, { useEffect } from "react";
import { Grid } from "@mui/material/index";
import { Paper } from "@mui/material/index";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/app/redux/store";
import { estateDetails } from "@/app/redux/slice/estateSlice";
import { Property } from "@/app/types/Estate";
interface PageProps {
  estateId: number;
  params: {
    estateId: string;
  };
}
const Page = ({ params }:{params:PageProps}) => {
  const estateId = params.estateId;
  const dispatch:AppDispatch = useDispatch();
  const singleEstate:Property = useSelector((state: RootState) => state.estates.estate);

  useEffect(() => {
    const handleEstate = () => {
      dispatch(estateDetails(estateId));
    };
    handleEstate();
  }, [dispatch, estateId]);
  return (
    <Grid
      container
      alignContent="center"
      justifyContent="center"
      sx={{
        width: "100vw",
        height: "auto",
        padding: "25px",
      }}
    >
      <Paper
        elevation={10}
        sx={{
          width: "30%",
          height: "60vh",
          padding: "20px",
          borderRadius: "10px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <h5> {singleEstate?.owner?.name} </h5>
        <span>{singleEstate?.owner?.contact} </span>
        <span> {singleEstate?.owner?.phone} </span>
      </Paper>
    </Grid>
  );
};

export default Page;
