import React from 'react';
import {  Grid } from '@mui/material';
import FilterBox from '../components/FilterBox';
import EstateList from '../components/EstateLIst';

const Page = () => {
  return (
    <Grid container sx={{ minHeight: 'auto' }}>
      <Grid
        item
        xs={1.5}
        sx={{
          borderRight: '1px solid #ccc',
          padding: '10px',
          boxShadow: '2px 0px 5px rgba(0, 0, 0, 0.1)', 
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <FilterBox />
      </Grid>
      <Grid minHeight={"80vh"} item xs={10.5} padding="20px" display="flex" flexDirection="column" justifyContent="space-between">
      <EstateList/>
      </Grid> 
     
    </Grid>
  );
};

export default Page;
