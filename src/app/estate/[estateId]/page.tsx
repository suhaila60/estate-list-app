'use client'
import React, { useEffect } from 'react';
import { Grid, Paper, Button } from '@mui/material';
import Image from 'next/image';  
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '@/app/redux/store';
import { estateDetails } from '@/app/redux/slice/estateSlice';
import { Property } from '@/app/types/Estate';
interface PageProps {
  estateId: number;
  params: {
    estateId: string;
  };
}
const EstateDetailPage = ({ params }:{params:PageProps}) => {
  const estateId = params.estateId;

  const dispatch:AppDispatch = useDispatch();
  const singleEstate:Property = useSelector((state: RootState) => state.estates.estate);

  useEffect(() => {
    const handleEstate = () => {
      dispatch(estateDetails(estateId));
    };
    handleEstate();
  }, [dispatch, estateId]);

  return (
    <Grid container spacing={3} sx={{ minHeight: '90vh' }}>
      <Grid item xs={12} sm={5}>
        <Paper elevation={3} style={{ padding: '20px', textAlign: 'center', height: '100%' }}>
          {singleEstate?.images?.map((image, index) => (
            <Image
              key={index}
              src={image}
              alt={`estate-image-${index}`}
              width={400}
              height={300} 
            />
          ))}
        </Paper>
      </Grid>
      <Grid container xs={12} sm={7}>
        <Grid item xs={12} sx={{ height: '90%', padding: '20px' }}>
          <Paper elevation={3} style={{ padding: '20px', height: '100%' }}>
            <h4>{singleEstate?.location?.city}</h4>
            <h4>{singleEstate?.location?.state}</h4>
            <h4>{singleEstate?.location?.country}</h4>
           <p>{singleEstate?.description}</p>
            <p>{singleEstate?.price}</p>

          </Paper>
        </Grid>
        <Grid item xs={12} sx={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Button variant="contained" sx={{ backgroundColor: 'primary.dark' }} href={`/estate/contact-agent/${estateId}`}>
            Contact Agent
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default EstateDetailPage;

