import React from "react";
import { Grid } from "../../../node_modules/@mui/material/index";

const layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <Grid
      container
      sx={{
        display: "flex",
        flexDirection: "column",
        minHeight: "auto",
      }}
    >
      {children}
    </Grid>
  );
};

export default layout;
