import React from "react";
import { Box } from "@mui/material/index";
const layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <Box
      sx={{
        backgroundImage: ' url("/estat.jpg")',
        backgroundSize: "cover",
        backgroundPosition: " center",
        minHeight: "auto",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: "10px",
      }}
    >
      {children}
    </Box>
  );
};

export default layout;
