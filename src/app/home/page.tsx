import React from 'react';
import { Box, Button } from '@mui/material';
import HomeButtonLinks from '../components/HomeButtonLinks';
import HomeServiceSection from '../components/HomeServiceSection';
import HomeContactSection from '../components/HomeContactSection';

const HomePage = () => {
  return (
    <>
    <HomeButtonLinks/>
      <Box
        sx={{
          padding: '20px',
          border: 'solid white 1px ',
          backgroundColor: 'secondary.light',
          marginTop: '80px',
          color: 'primary.light',
          borderRadius: '10px',
          width: '40%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          textAlign: 'center',
          lineHeight: 2,
        }}
      >
        <h2>Discover Your Dream Property</h2>
        <p>
          Discover the perfect property for your needs. Whether you're looking
          for a cozy home or a spacious commercial space, we have a variety of
          options to explore.
        </p>
      </Box>
    
     <HomeServiceSection/>
     <HomeContactSection/>
    </>
  );
};

export default HomePage;
