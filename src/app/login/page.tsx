"use client";
import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material/index";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../redux/store";
import { getRegisterUser } from "../redux/slice/registerSlice";
import { useRouter } from "next/navigation";
import RegisterUser from "../types/RegisterUser";

const Page = () => {
  const router = useRouter();
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [error, setError] = useState<string>("");
  const dispatch: AppDispatch = useDispatch();
  const users: RegisterUser[] = useSelector(
    (state: RootState) => state.registrations.registration
  );

  useEffect(() => {
    const handleEstate = () => {
      dispatch(getRegisterUser());
    };
    handleEstate();
  }, [dispatch]);

  const handleLogin = async () => {
    try {
      const user = users.find(
        (user) => user.username === username && user.password === password
      );
      if (user) {
        router.push("/home");
      } else {
        setError("Invalid credentials");
      }
    } catch (error) {
      setError("Login failed. Please try again later.");
    }
  };

  return (
    <Grid
      container
      alignContent="center"
      justifyContent="center"
      sx={{
        width: "100vw",
        height: "auto",
        padding: "25px",
      }}
    >
      <Paper
        elevation={10}
        sx={{
          width: "30%",
          height: "60vh",
          padding: "20px",
          borderRadius: "10px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <TextField
          required
          id="outlined-basic"
          label="user name"
          variant="outlined"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />

        <TextField
          required
          type="password"
          id="outlined-required"
          label="password"
          variant="outlined"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button
          onClick={handleLogin}
          sx={{ width: "150px" }}
          variant="contained"
        >
          Login
        </Button>
        {error && <Typography color="error">{error}</Typography>}
        <span>or register </span>
        <Link href={"/register"}>register</Link>
      </Paper>
    </Grid>
  );
};

export default Page;
