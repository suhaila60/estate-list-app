interface RegisterUser {
    id: string;
    username: string;
    email: string;
    password: string;
  }
  
  export default RegisterUser;
  