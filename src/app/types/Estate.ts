interface Location {
    city: string;
    state: string;
    country: string;
    latitude: number;
    longitude: number;
  }
  
  interface Owner {
    name: string;
    contact: string;
    phone?: string;
  }
  
export  interface Property {
    id?:  number ;
    location?: Location;
    images?: string[];
    price?: string;
    description?: string;
    features?: string[];
    owner?: Owner;
  }
  
  
