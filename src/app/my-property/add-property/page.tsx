'use client'
import React, { useState } from "react";
import { Grid } from "@mui/material";
import { Paper, TextField, Button } from "@mui/material";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { useDispatch } from "react-redux";
import { createProperty } from "../../redux/slice/myPropertySlice";
import { addEstates } from "@/app/redux/slice/estateSlice";
import AddPropertyButton from "@/app/components/AddPropertyButton";

interface CreateProperty {
  id: string;
  type: string;
  location: {
    city: string;
    state: string;
    country: string;
    latitude: number;
    longitude: number;
  };
  images: string[];
  price: string;
  description: string;
  features: string[];
  owner: {
    name: string;
    contact: string;
    phone: string;
  };
}

const AddProperty = () => {
  const dispatch = useDispatch();
  const [type, setType] = React.useState("");

  const handleChange = (event: SelectChangeEvent) => {
    setType(event.target.value as string);
  };

  const [inputData, setInputData] = useState<CreateProperty>({
    id: "",
    type: "",
    location: {
      city: "",
      state: "",
      country: "",
      latitude: 0,
      longitude: 0,
    },
    images: [],
    price: "",
    description: "",
    features: [],
    owner: {
      name: "",
      contact: "",
      phone: "",
    },
  });

  const handleDataCreate = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = event.target;

    if (id === "type") {
      setInputData({
        ...inputData,
        type: value,
      });
    } else if (id === "imageUrls") {
      const imageUrls = value.split(",");
      setInputData({
        ...inputData,
        images: imageUrls,
      });
    } else if (id === "location") {
      const [city, state, country] = value.split(",");
      setInputData({
        ...inputData,
        location: {
          ...inputData.location,
          city,
          state,
          country,
        },
      });
    } else if (id.startsWith("features")) {
      const features = value.split(",");
      setInputData({
        ...inputData,
        features,
      });
    } else if (id.startsWith("owner")) {
      const [name, contact, phone] = value.split(",");
      setInputData({
        ...inputData,
        owner: {
          ...inputData.owner,
          name,
          contact,
          phone,
        },
      });
    } else {
      setInputData({
        ...inputData,
        [id]: value,
      });
    }
  };
  const handleAddProperty = () => {
       dispatch(createProperty(inputData));
    dispatch(addEstates(inputData));
  };

  return (
    <Grid
      container
      alignContent="center"
      justifyContent="center"
      sx={{
        width: "100vw",
        height: "auto",
        padding: "25px",
      }}
    >
      <Paper
        elevation={10}
        sx={{
          width: "70%",
          height: "auto",
          padding: "20px",
          borderRadius: "10px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <Grid container spacing={4} justifyContent="center">
          <Grid item xs={12} sm={5}>
            <Box sx={{ minWidth: 120 }}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Choose type</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="type"
                  value={type}
                  label="Choose type"
                  onChange={handleChange}
                >
                  <MenuItem value="house">House</MenuItem>
                  <MenuItem value="apartment">Apartment</MenuItem>
                  <MenuItem value="condo">Land</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="location"
              label="Enter location (city, state, country)"
              variant="outlined"
              fullWidth
              onChange={handleDataCreate}

            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="imageUrls"
              label="Enter images URLs (comma-separated)"
              variant="outlined"
              fullWidth
              multiline
              onChange={handleDataCreate}
            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="price"
              label="Enter price"
              variant="outlined"
              fullWidth
              onChange={handleDataCreate}
            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="description"
              label="Enter description"
              variant="outlined"
              fullWidth
              multiline
              onChange={handleDataCreate}
            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="features"
              label="Enter features (comma-separated)"
              variant="outlined"
              fullWidth
              multiline
              onChange={handleDataCreate}
            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="owner"
              label="Enter owner info (name, contact, phone)"
              variant="outlined"
              fullWidth
              multiline
              onChange={handleDataCreate}
            />
          </Grid>
          <Grid item xs={12} sm={5}>
            <TextField
              required
              id="id"
              label="Add 4 digit number as id"
              variant="outlined"
              fullWidth
              onChange={handleDataCreate}
            />
          </Grid>
        </Grid>
<AddPropertyButton inputData={inputData}  />
       
      </Paper>
    </Grid>
  );
};

export default AddProperty;
