'use client'
import React, { useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import Paper from "@mui/material/Paper";
import TableRow from "@mui/material/TableRow";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch, RootState } from "../redux/store";
import { deleteFavoriteItem, getFavorites } from "../redux/slice/favoriteSlice";
import { Box, Typography } from "@mui/material/index";
import MyPropertyCard from "../components/MyPropertyCard";
import { Property } from "../types/Estate";
import { getMyProperties } from "../redux/slice/myPropertySlice";
import Loadings from "../components/Loading";

const MyProperty = () => {
  const dispatch: AppDispatch = useDispatch();
  const { myProperty, isLoading, error } = useSelector(
    (state: RootState) => state.myProperties
  );

  useEffect(() => {
    const handleMyProperty = () => {
      dispatch(getMyProperties());
    };
    handleMyProperty();
  }, [dispatch]);

  const handleClick = async (favoriteId: number) => {
    dispatch(deleteFavoriteItem(favoriteId));
  };

  if (isLoading) {
    return <Loadings/>;
  }

  if (error) {
    return <Typography>Error: {error}</Typography>;
  }

  return (
    <Box display="flex" justifyContent="center" padding="20px">
      <TableContainer component={Paper} sx={{ width: "70%" }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>
                <h3>Image</h3>
              </TableCell>
              <TableCell>
                <h3>Location</h3>
              </TableCell>
              <TableCell>
                <h3>Price</h3>
              </TableCell>
              <TableCell>
                <h3>Remove</h3>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Array.isArray(myProperty) &&
              myProperty?.map((property) => (
                <MyPropertyCard
                  key={property?.id}
                  property={property}
                  handleClick={handleClick}
                />
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default MyProperty;
